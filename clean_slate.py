from selenium import webdriver
import requests
import time
from webdriver_manager.chrome import ChromeDriverManager
from dotenv import load_dotenv
from os import getenv
from selenium.webdriver.chrome.options import Options

options = Options()
if getenv("CLEAN_SLATE_DONT_WAIT_FOR_CAPTCHA"):
    options.headless = True

load_dotenv()


site = 'https://cjs.shelbycountytn.gov/CJS/Account/Login'
driver = webdriver.Chrome(ChromeDriverManager().install(), options=options)

name_or_case_number = 'smith, john'
date_of_birth = "11/07/1981"

if getenv("CLEAN_SLATE_USERNAME"):
    username = getenv("CLEAN_SLATE_USERNAME")
else:
    username = input('Please enter your user name to log in: ')
if getenv("CLEAN_SLATE_PASSWORD"):
    password = getenv("CLEAN_SLATE_PASSWORD")
else:
    password = input('As well as your password: ')
##name_or_case_number = input("Kindly enter a name in 'last, first' form OR enter a case number please.")
##date_of_birth = input("Once more, please enter a DOB in MM/DD/YYYY form. Thank you!!")


def login_portal(username, password):
    time.sleep(3)
    driver.get(site)
    time.sleep(3)

    ## User Info entry and click login
    user_name_entry = driver.find_element_by_id('UserName')
    user_name_entry.send_keys(username)
    password_entry = driver.find_element_by_id('Password')
    password_entry.send_keys(password)
    login_click = driver.find_element_by_css_selector('.btn')
    login_click.click()
    time.sleep(2)
    search_site = 'https://cjs.shelbycountytn.gov/CJS/Home/Dashboard/29'
    driver.get(search_site)



## Here for multiple case numbers
def case_numbers():
    case_links = driver.find_elements_by_class_name('caseLink')
    for i in range(len(case_links)):
        case_links[i].click()
    ##Here is where the downloading starts
        time.sleep(5)
        scrape_case_number = driver.find_element_by_xpath("//div[@id='divCaseInformation_body']//span[contains(text(), 'Case Number')]/parent::*")
        print(scrape_case_number.text)
        disp_number = driver.find_elements_by_id('DispositionEventsPrintSection')
        for i in range(len(disp_number)):
            print(disp_number[i].text)
    ##This goes back to search results
        time.sleep(5)
        back_to_search = driver.find_element_by_xpath("//p[contains(text( ), 'Search Results')]")
        back_to_search.click()
        time.sleep(3)

## Enter search of site
def search_and_expunge():

    time.sleep(5)
    record_search = driver.find_element_by_id('caseCriteria_SearchCriteria')
    record_search.send_keys(name_or_case_number)
    click_submit = driver.find_element_by_id("btnSSSubmit")
    click_submit.click()


    ## Entering in DOB
    time.sleep(5)
    dob_drop_down = driver.find_element_by_xpath("//th[@data-field='DateOfBirthSort']//span[@title='Sort / Filter Options']")
    dob_drop_down.click()
    time.sleep(5)
    dob_text_box = driver.find_element_by_xpath("//div[@class='k-animation-container']//input[@class='k-input']")
    dob_text_box.click()
    dob_text_box.send_keys(date_of_birth)
    dob_submit = driver.find_element_by_xpath("//div[@class='k-animation-container']//button[@type='submit']")
    dob_submit.click()

    


    ##From name search results (after DOB is entered), gets to info to be downloaded
    ##From here I can find all the caseLinks and store them in a list, and then loop thru each.
    ##Each going click case# (@detailed), scrape info, press back, find next case#, repeat
    time.sleep(5)
    case_numbers()






login_portal(username, password)
if not getenv("CLEAN_SLATE_DONT_WAIT_FOR_CAPTCHA"):
    input("Hit enter when you've completed the captcha.")
else:
    print("If you get a Captcha, configure your run differently to wait for it.")
search_and_expunge()





##RN I can get relevant text by search by id the 'disp_number = driver.find_elements_by_id('DispositionEventsPrintSection')
##I'm sure I can get even more refined by only looking for the text that's within that block, under a couple diff headers
##Tjat should give me enough to only take what I'd like. FULL CODE:

##for i in range(len(disp_number)):
##	print(disp_number[i].text)
